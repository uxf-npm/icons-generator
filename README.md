# @uxf/icons-generator

## Quick start

### Install
```
npm i -D @uxf/icons-generator
```

or use yarn
```
yarn add -D @uxf/icons-generator
```

### Create simple JS config file (`icons.config.js`) in your root folder
```js
module.exports = {
    icons: {
        test: {
            width: 24,
            height: 24,
            data: `<path fill="#fecd09" d="M33.03 17.44c-1.68 0-3.12-.99-3.79-2.42h7.58a4.181 4.181 0 01-3.79 2.42z" />`
        }
    }
}
```
Full list of config options is [here](#config).

### Run generator
```
icons-gen
```

Or, if you need to use different name for config file:
```
icons-gen --configFile=yourConfigFileName.js
```

## Config

| key                     | type                         | default                                                               | required | description                         |
|-------------------------|------------------------------|-----------------------------------------------------------------------|----------|-------------------------------------|
| mode                    | `"simple" \                  | "sizes"`                                                              | `"simple"`                   |                                     |                                   |
| typescript              | `boolean`                    | `true`                                                                |          |                                     |
| configDirectory         | `string`                     | `"/src/config/"`                                                      |          |                                     |
| generatedDirectory      | `string`                     | `"/public/icons-generated/"`                                          |          |                                     |
| spriteFileName          | `string`                     | `undefined`                                                           |          | custom content to definition file   |
| typeName                | `string`                      | `IconsSet`                                                             |          |                                     |
| moduleDefinition        | `ModuleDefinition`           | `"{ moduleName: "icons", typeName: "IconsSet", format: "interface"}"` |                                                  | allows to generate module definiton |
| customDefinitionContent | `string`                     | `"_icon-sprite.svg"`                                                  |          |                                     |
| icons                   | `Record<string, SimpleIcon \ | SizedIcon>`                                                           |                              | yes                                 | find out more [here](#icon-types) |

## Icon types
### SimpleIcon (for mode "simple")
#### Type
```ts
type SimpleIcon = {
    data: string;
    height: number;
    width: number;
};
```

#### Example
```js
test: {
    width: 50,
    height: 60,
    data: `<path fill="#fecd09" d="M33.03 17.44c-1.68 0-3.12-.99-3.79-2.42h7.58a4.181 4.181 0 01-3.79 2.42z" />`,
}
```

### SizedIcon (for mode "sizes")
#### Type
```ts
type SizedIcon = Record<number, string>;
```

#### Example
```js
test: {
    24: `<path fill="#fecd09" d="M33.03 17.44c-1.68 0-3.12-.99-3.79-2.42h7.58a4.181 4.181 0 01-3.79 2.42z" />`,
    44: `<path fill="#fecd09" d="M33.03 17.44c-1.68 0-3.12-.99-3.79-2.42h7.58a4.181 4.181 0 01-3.79 2.42z" />`,
}
```

## Recommended use (step-by-step)
### 1. Create config file in root folder of your project...

... and configure all icons you need.

### 2. Generate icons

Run `icons-gen` to generate:

- SVG sprite file with all icons
- Separate SVG files for each icon
- "Definition file" which contains list of all icons and their sizes
- "Config file" which contains current icons version and path to generated sprite file

### 3. Setup your Icon component

Implement generator output to your project structure.
Example for `typescript` and `React`:

```tsx
import { ICONS, IconsSet } from "src/config/icons";
import { ICON_SPRITE } from "src/config/icons-config";

type IconProps = {
    name: IconsSet;
};

export const Icon: FC<IconProps> = ({ name }) => {
    const sizes = ICONS[name];

    return (
        <svg
            height={sizes.h}
            width={sizes.w}
            preserveAspectRatio="xMidYMid meet"
            role="img"
            viewBox={`0 0 ${sizes.w} ${sizes.h}`}
        >
            <use xlinkHref={`${ICON_SPRITE}#icon-sprite--${name}`} />
        </svg>
    );
};
```

### 4. Preload your sprite (optional)

```tsx
import { ICON_SPRITE, ICONS_VERSION } from "src/config/icons-config";

// code here...

<Head>
    <link
        as="image"
        href={`${ICON_SPRITE}?v=${ICONS_VERSION}`}
        rel="preload"
        type="image/svg+xml"
    />
</Head>
```

### 5. Check it

To be sure, you have generated the last version of icons, run:
```
icons-check
```
This command compares icons with config file and fails, when icons doesn't match current config.
We strongly recommend add this command to your CI.
