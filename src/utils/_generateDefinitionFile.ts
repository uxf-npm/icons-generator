import { writeFileSync } from "fs";
import { Mode, ModuleDefinition } from "../types";
import { _createDefinitionFileContent } from "./_createDefinitionFileContent";
import { _createPath } from "./_createPath";

export function _generateDefinitionFile({
    source,
    resultFile,
    typescript,
    mode,
    typeName,
    moduleDefinition,
    customContent,
}: {
    source: any;
    resultFile: string;
    typescript: boolean;
    mode: Mode;
    typeName: string;
    moduleDefinition?: ModuleDefinition;
    customContent?: string;
}) {
    const newIcons = _createDefinitionFileContent({
        source,
        typescript,
        mode,
        typeName,
        moduleDefinition,
        customContent,
    });

    _createPath(resultFile);
    writeFileSync(resultFile, newIcons);
}
