import { readdirSync, unlinkSync } from "fs";
import { _createPath } from "./_createPath";
import { SimpleIcon, SizedIcon } from "../types";

export function _removeUnusedSeparateIconFiles({
    source,
    directory,
    spriteFileName,
}: {
    source: Record<string, SimpleIcon | SizedIcon>;
    directory: string;
    spriteFileName: string;
}) {
    _createPath(directory, false);

    const dir = readdirSync(directory, { withFileTypes: true });

    dir.map(file => {
        if (file.isFile() && !(file.name in source) && file.name !== spriteFileName) {
            unlinkSync(directory + file.name);
        }
    });
}
