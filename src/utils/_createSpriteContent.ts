import { Mode } from "../types";

export function _createSpriteContent({ source, mode }: { source: any; mode: Mode }): string {
    let spriteTextContent = `<svg xmlns="http://www.w3.org/2000/svg">`;

    switch (mode) {
        case Mode.Simple:
            Object.keys(source).map(name => {
                const svg = source[name];

                spriteTextContent += `<symbol id="icon-sprite--${name}" viewBox="0 0 ${svg.width} ${svg.height}">`;
                spriteTextContent += svg.data;
                spriteTextContent += `</symbol>`;
            });
            break;
        case Mode.Sizes:
            Object.keys(source).map(name => {
                const sizes = source[name];
                return Object.keys(sizes).map(s => {
                    const svg = sizes[s];

                    spriteTextContent += `<symbol id="icon-sprite--${name}_${s}" viewBox="0 0 ${s} ${s}">${svg}</symbol>`;
                });
            });
            break;
        default:
            throw new Error("Unknown mode " + mode);
    }

    spriteTextContent += `</svg>`;

    return spriteTextContent;
}
