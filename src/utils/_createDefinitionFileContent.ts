import { Mode, ModuleDefinition, ModuleDefinitionFormat } from "../types";

export function _createDefinitionFileContent({
    source,
    typescript,
    mode,
    typeName,
    moduleDefinition,
    customContent,
}: {
    source: any;
    typescript: boolean;
    mode: Mode;
    typeName: string;
    moduleDefinition?: ModuleDefinition;
    customContent?: string;
}) {
    let size: number;
    let newIcons = `// this file is generated automatically, do not change anything manually in the contents of this file\n\nexport const ICONS = {\n`;

    Object.keys(source).map(name => {
        switch (mode) {
            case Mode.Simple:
                newIcons += `    "${name}": { w: ${source[name].width}, h: ${source[name].height} },\n`;
                break;
            case Mode.Sizes:
                size = source[name];
                newIcons += `    "${name}": [${Object.keys(size).join(", ")}],\n`;
                break;
            default:
                throw new Error("Unknown mode " + mode);
        }
    });

    newIcons += "}";
    if (typescript) {
        newIcons += ` as const;\n\nexport type ${typeName} = keyof typeof ICONS;\n`;

        if (moduleDefinition) {
            newIcons += `\ndeclare module "${moduleDefinition.moduleName}" {\n`;
            switch (moduleDefinition.format) {
                case ModuleDefinitionFormat.Interface:
                    newIcons += `    interface ${moduleDefinition.typeName} {\n`;
                    Object.keys(source).map(name => {
                        newIcons += `        "${name}": true;\n`;
                    });
                    newIcons += `    }\n`;
                    break;
                case ModuleDefinitionFormat.Type:
                    newIcons += `    type ${moduleDefinition.typeName} = "${Object.keys(source).join('" | "')}";\n`;
                    break;
                default:
                    throw new Error("Unknown mode " + mode);
            }
            newIcons += "}\n";
        }
    }

    if (customContent) {
        newIcons += `\n${customContent}`;
    }

    return newIcons;
}
