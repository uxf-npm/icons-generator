import { existsSync, readFileSync } from "fs";
import { Mode } from "../types";
import { _createSpriteContent } from "./_createSpriteContent";

export function _compareSprites({ source, spriteFile, mode }: { source: any; spriteFile: string; mode: Mode }) {
    const spriteTextContent = _createSpriteContent({ source, mode });
    const oldSvgContent = existsSync(spriteFile) ? readFileSync(spriteFile).toString() : "";

    return spriteTextContent === oldSvgContent;
}
