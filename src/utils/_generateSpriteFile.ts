import { writeFileSync } from "fs";
import { Mode } from "../types";
import { _createPath } from "./_createPath";
import { _createSpriteContent } from "./_createSpriteContent";

export function _generateSpriteFile({ source, spriteFile, mode }: { source: any; spriteFile: string; mode: Mode }) {
    _createPath(spriteFile);
    writeFileSync(spriteFile, _createSpriteContent({ source, mode }));
}
