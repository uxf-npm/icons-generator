import { existsSync, mkdirSync } from "fs";

export function _createPath(path: string, isFile = true): void {
    const arr = path.split("/");
    if (isFile) {
        arr.pop();
    }
    const dirPath = arr.join("/");

    if (!existsSync(dirPath)) {
        mkdirSync(dirPath, { recursive: true });
    }
}
