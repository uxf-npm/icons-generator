import { Config, Mode, ModuleDefinition } from "../types";

function _getModuleDefinition(data: any): ModuleDefinition | undefined {
    if (!data) {
        return undefined;
    }

    return {
        moduleName: data.moduleName || "icons",
        typeName: data.typeName || "IconsSet",
        format: data.format || "interface",
    };
}

export function _getConfig(): Config {
    const confFile = process.argv.filter(c => c.includes("--configFile="))[0]?.split("=")[1];

    /* eslint-disable-next-line @typescript-eslint/no-var-requires */
    const loadedConfig: Record<string, any> = require(process.cwd() + "/" + (confFile || "icons.config.js"));

    const config = {
        configDirectory: loadedConfig.configDirectory || "/src/config/",
        icons: loadedConfig.icons || {},
        mode: loadedConfig.mode || Mode.Simple,
        spriteFileName: loadedConfig.spriteFileName || "_icon-sprite.svg",
        typeName: loadedConfig.typeName || "IconsSet",
        typescript: loadedConfig.typescript || true,
        customDefinitionContent: loadedConfig.customDefinitionContent || undefined,
        moduleDefinition: _getModuleDefinition(loadedConfig.moduleDefinition),
    };

    const ext = config.typescript ? "ts" : "js";
    const definitionFile = process.cwd() + config.configDirectory + `icons.${ext}`;
    const configFile = process.cwd() + config.configDirectory + `icons-config.${ext}`;
    const generatedRelativeDirectory = loadedConfig.generatedDirectory || "/public/icons-generated/";
    const generatedDirectory = process.cwd() + generatedRelativeDirectory;
    const spriteFile = generatedDirectory + "/" + config.spriteFileName;

    return {
        ...config,
        configFile,
        definitionFile,
        ext,
        generatedDirectory,
        generatedRelativeDirectory,
        spriteFile,
    };
}
