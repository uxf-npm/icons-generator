export function _createConfigFileContent({
    spriteFileName,
    generatedRelativeDirectory,
}: {
    spriteFileName: string;
    generatedRelativeDirectory: string;
}) {
    const iconsVersion = new Date().getTime();
    return `export const ICONS_VERSION = "${iconsVersion}";\nexport const ICON_SPRITE = "${generatedRelativeDirectory.replace(
        "/public",
        "",
    )}${spriteFileName}";\n`;
}
