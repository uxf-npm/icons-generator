import { existsSync, readFileSync } from "fs";
import { Mode, ModuleDefinition } from "../../types";
import { _createDefinitionFileContent } from "../_createDefinitionFileContent";

export function _validateDefinitions({
    source,
    resultFile,
    typescript,
    mode,
    typeName,
    moduleDefinition,
    customContent,
}: {
    source: any;
    resultFile: string;
    typescript: boolean;
    mode: Mode;
    typeName: string;
    moduleDefinition?: ModuleDefinition;
    customContent?: string;
}): void {
    const newContent = _createDefinitionFileContent({
        source,
        typescript,
        mode,
        typeName,
        moduleDefinition,
        customContent,
    });

    const oldContent = existsSync(resultFile) ? readFileSync(resultFile).toString() : "";

    if (newContent !== oldContent) {
        throw new Error("Definition file doesn't contain all valid icons from config");
    }
}
