import { validate } from "fast-xml-parser";
import { Mode } from "../../types";

export function _validateSourceData({ source, mode }: { source: any; mode: Mode }): void {
    switch (mode) {
        case Mode.Simple:
            Object.keys(source).map(name => {
                const svg = source[name];

                if (validate(svg.data) !== true) {
                    throw new Error(`Invalid SVG data for icon "${name}"`);
                }
            });
            break;
        case Mode.Sizes:
            Object.keys(source).map(name => {
                const sizes = source[name];
                return Object.keys(sizes).map(s => {
                    const svg = sizes[s];

                    if (validate(svg) !== true) {
                        throw new Error(`Invalid SVG data for icon "${name}"`);
                    }
                });
            });
            break;
        default:
            throw new Error("Unknown mode " + mode);
    }
}
