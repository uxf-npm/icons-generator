import { Config } from "../../types";
import { _validateConfigFile } from "./_validateConfigFile";
import { _validateDefinitions } from "./_validateDefinitions";
import { _validateSeparateIcons } from "./_validateSeparateIcons";
import { _validateSourceData } from "./_validateSourceData";
import { _validateSprite } from "./_validateSprite";

export function _check(config: Config) {
    _validateSourceData({ source: config.icons, mode: config.mode });
    _validateSprite({ source: config.icons, spriteFile: config.spriteFile, mode: config.mode });
    _validateDefinitions({
        source: config.icons,
        resultFile: config.definitionFile,
        typescript: config.typescript,
        mode: config.mode,
        typeName: config.typeName,
        moduleDefinition: config.moduleDefinition,
        customContent: config.customDefinitionContent,
    });
    _validateSeparateIcons({ source: config.icons, generatedDir: config.generatedDirectory, mode: config.mode });
    _validateConfigFile({
        configFile: config.configFile,
        spriteFileName: config.spriteFileName,
        generatedRelativeDirectory: config.generatedRelativeDirectory,
    });
}
