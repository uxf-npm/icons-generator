import { existsSync, readFileSync } from "fs";
import { Mode } from "../../types";
import { _createIconContent } from "../_createIconContent";

export function _validateSeparateIcons({
    source,
    generatedDir,
    mode,
}: {
    source: any;
    generatedDir: string;
    mode: Mode;
}) {
    switch (mode) {
        case Mode.Simple:
            Object.keys(source).map(name => {
                const svg = source[name];
                const content = _createIconContent({ width: svg.width, height: svg.height, content: svg.data });
                const oldContent = existsSync(generatedDir + name + ".svg")
                    ? readFileSync(generatedDir + name + ".svg").toString()
                    : "";

                if (content !== oldContent) {
                    throw new Error(`SVG file for icon "${name}" has no valid content for current config`);
                }
            });
            break;
        case Mode.Sizes:
            Object.keys(source).map(name => {
                const size = source[name];
                Object.keys(size).map(s => {
                    const svg = size[s];
                    const content = _createIconContent({ width: s, height: s, content: svg });
                    const oldContent = existsSync(generatedDir + name + "_" + s + ".svg")
                        ? readFileSync(generatedDir + name + "_" + s + ".svg").toString()
                        : "";

                    if (content !== oldContent) {
                        throw new Error(`SVG file for icon "${name}" has no valid content for current config`);
                    }
                });
            });
            break;
        default:
            throw new Error("Unknown mode " + mode);
    }
}
