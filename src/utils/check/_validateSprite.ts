import { Mode } from "../../types";
import { _compareSprites } from "../_compareSprites";

export function _validateSprite({ source, spriteFile, mode }: { source: any; spriteFile: string; mode: Mode }): void {
    if (!_compareSprites({ source, mode, spriteFile })) {
        throw new Error("SVG file doesn't contain all valid icons from config");
    }
}
