import { existsSync, readFileSync } from "fs";
import { _createConfigFileContent } from "../_createConfigFileContent";

function normalize(content: string): string {
    return content.replace(/ICONS_VERSION = "[0-9]+"/g, "%%VERSION%%");
}

export function _validateConfigFile({
    spriteFileName,
    generatedRelativeDirectory,
    configFile,
}: {
    spriteFileName: string;
    generatedRelativeDirectory: string;
    configFile: string;
}): void {
    const newContent = _createConfigFileContent({
        spriteFileName,
        generatedRelativeDirectory,
    });

    const oldContent = existsSync(configFile) ? readFileSync(configFile).toString() : "";

    if (normalize(newContent) !== normalize(oldContent)) {
        throw new Error("Config file doesn't contain all valid information");
    }
}
