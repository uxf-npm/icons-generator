import { writeFileSync } from "fs";
import { _createConfigFileContent } from "./_createConfigFileContent";
import { _createPath } from "./_createPath";

export function _generateConfigFile({
    configFile,
    spriteFileName,
    generatedRelativeDirectory,
}: {
    configFile: string;
    spriteFileName: string;
    generatedRelativeDirectory: string;
}) {
    _createPath(configFile);
    writeFileSync(
        configFile,
        _createConfigFileContent({
            spriteFileName: spriteFileName,
            generatedRelativeDirectory: generatedRelativeDirectory,
        }),
    );
}
