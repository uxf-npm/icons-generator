import { writeFileSync } from "fs";
import { Mode } from "../types";
import { _createPath } from "./_createPath";
import { _createIconContent } from "./_createIconContent";

export function _generateSeparateIconFiles({
    source,
    directory,
    mode,
}: {
    source: any;
    directory: string;
    mode: Mode;
}) {
    _createPath(directory, false);
    switch (mode) {
        case Mode.Simple:
            Object.keys(source).map(name => {
                const svg = source[name];
                const content = _createIconContent({ width: svg.width, height: svg.height, content: svg.data });
                writeFileSync(directory + name + ".svg", content);
            });
            break;
        case Mode.Sizes:
            Object.keys(source).map(name => {
                const size = source[name];
                Object.keys(size).map(s => {
                    const svg = size[s];
                    const content = _createIconContent({ width: s, height: s, content: svg });
                    writeFileSync(directory + name + "_" + s + ".svg", content);
                });
            });
            break;
        default:
            throw new Error("Unknown mode " + mode);
    }
}
