export enum Mode {
    Simple = "simple",
    Sizes = "sizes",
}

export enum ModuleDefinitionFormat {
    Type = "type",
    Interface = "interface",
}

export type ModuleDefinition = {
    moduleName: string;
    typeName: string;
    format: ModuleDefinitionFormat;
};

export type SimpleIcon = {
    data: string;
    height: number;
    width: number;
};

export type SizedIcon = Record<number, string>;

type IconsByMode =
    | {
          icons: Record<string, SimpleIcon>;
          mode: Mode.Simple;
      }
    | {
          icons: Record<string, SizedIcon>;
          mode: Mode.Sizes;
      };

export type Config = {
    configDirectory: string;
    configFile: string;
    customDefinitionContent?: string;
    definitionFile: string;
    moduleDefinition?: ModuleDefinition;
    ext: "ts" | "js";
    generatedDirectory: string;
    generatedRelativeDirectory: string;
    spriteFile: string;
    spriteFileName: string;
    typeName: string;
    typescript: boolean;
} & IconsByMode;
