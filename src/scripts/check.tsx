#!/usr/bin/env node
import { _getConfig } from "../utils/_getConfig";
import { _check } from "../utils/check/_check";

const config = _getConfig();

let error = false;

try {
    _check(config);
} catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
    error = true;
}

if (!error) {
    // eslint-disable-next-line no-console
    console.log("Everything OK!");
}

process.exit(error ? 1 : 0);
