#!/usr/bin/env node
import { _generateConfigFile } from "../utils/_generateConfigFile";
import { _generateDefinitionFile } from "../utils/_generateDefinitionFile";
import { _generateSeparateIconFiles } from "../utils/_generateSeparateIconFiles";
import { _generateSpriteFile } from "../utils/_generateSpriteFile";
import { _getConfig } from "../utils/_getConfig";
import { _check } from "../utils/check/_check";
import { _validateSourceData } from "../utils/check/_validateSourceData";
import { _removeUnusedSeparateIconFiles } from "../utils/_removeUnusedSeparateIconFiles";

const config = _getConfig();

let error = false;
let shouldUpdate = false;

try {
    _validateSourceData({ source: config.icons, mode: config.mode });

    try {
        _check(config);
    } catch (e) {
        shouldUpdate = true;
    }

    _validateSourceData({ source: config.icons, mode: config.mode });

    if (shouldUpdate) {
        _generateSpriteFile({ source: config.icons, spriteFile: config.spriteFile, mode: config.mode });
        // eslint-disable-next-line no-console
        console.log("Icon sprite file generated.");

        _generateDefinitionFile({
            source: config.icons,
            resultFile: config.definitionFile,
            typescript: config.typescript,
            mode: config.mode,
            typeName: config.typeName,
            moduleDefinition: config.moduleDefinition,
            customContent: config.customDefinitionContent,
        });
        // eslint-disable-next-line no-console
        console.log("Definition file generated.");

        _removeUnusedSeparateIconFiles({
            source: config.icons,
            directory: config.generatedDirectory,
            spriteFileName: config.spriteFileName,
        });
        _generateSeparateIconFiles({ source: config.icons, directory: config.generatedDirectory, mode: config.mode });
        // eslint-disable-next-line no-console
        console.log("Separate icons generated.");

        _generateConfigFile({
            configFile: config.configFile,
            spriteFileName: config.spriteFileName,
            generatedRelativeDirectory: config.generatedRelativeDirectory,
        });
        // eslint-disable-next-line no-console
        console.log("Config file generated.");
    } else {
        // eslint-disable-next-line no-console
        console.log("No changes in config, no icons generated.");
    }
} catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
    error = true;
}

process.exit(error ? 1 : 0);
