const TEST_REGEX = "(/tests/.*|(\\.|/)(test|spec))\\.ts$";
module.exports = {
    testRegex: TEST_REGEX,
    testPathIgnorePatterns: ["<rootDir>/.cache/", "<rootDir>/node_modules/", "<rootDir>/dist/", "<rootDir>/demo/"],
    moduleFileExtensions: ["ts", "tsx", "js", "jsx"],
    transform: {
        "^.+\\.ts$": "ts-jest",
    },
    collectCoverageFrom: ["<rootDir>/src/!(contexts)**/*.ts"],
    coverageDirectory: "_reports/coverage",
    coverageReporters: ["text", "cobertura", "html"],
    coverageThreshold: {
        global: {
            branches: 90,
            functions: 90,
            lines: 90,
            statements: 90,
        },
    },
};
