// eslint-disable-next-line no-undef
module.exports = {
    extends: ["@uxf/eslint-config/light"],
    ignorePatterns: ["dist/", "tests/"],
};
